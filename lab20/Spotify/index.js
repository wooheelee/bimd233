var my_spotify_url = "https://api.spotify.com/v1/users/qybnsjpvc2l1xawpss807i64y"
var oAuthToken = "BQBSJT5SbQaS5HO1t3uXLtpLJbEPLAHDRBISqPC_DgRGkpHdTPfYXGIzL-iFTYIAjtCEPqOd0MqBDuuB6DqJSQCONOhZ6zlR1qCG_nNsgehh5b33vAI73xKJ28C01Ca5iYeirng-A65gLQaDGaVFfbTP7IiSZLuh1ZRLpSooEma1yFhEGkoSakjAt9UuES5u9-YN"
// Hint: --by looking at how Spotify does this in their console from the above links
// NOTE!  The above oAuthToken is UNIQUE to YOUR Login Credentials

// The following is the solution pattern to the AJAX call to get the Spotify Search Result 
$.ajax({
  method: "GET",
  url: my_spotify_url,
  dataType: "json",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization: "Bearer " + oAuthToken
  },
  success: function(data) {
    console.log(data);
    document.getElementById("username").innerHTML = "User Name: " + data.display_name;
    document.getElementById("follwers").innerHTML = "Followers: " + data.followers.total;
    document.getElementById("type").innerHTML = "type: " + data.type;
    document.getElementById("url").innerHTML = "URL: " + data.external_urls.spotify;
  },
  error: function(e) {
    let err = JSON.stringify(e);
    console.log(err);
  },
  cache: false
});