$.ajax ({
  url: "https://api.themoviedb.org/3/movie/330457?api_key=b8492296b4985c713e81b86ee003dbf6",
  success: function(movie){
    document.getElementById("title").innerHTML = "Movie Title: " + movie.original_title;
    document.getElementById("overview").innerHTML = "Summary: " + movie.overview;
    document.getElementById("release").innerHTML = "Release Date: " + movie.release_date;
    document.getElementById("run").innerHTML = "Running Time: " + movie.runtime + " min";
  }
});