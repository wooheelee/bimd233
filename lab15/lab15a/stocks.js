var stock = [
    {
        name: "Microsoft",
        market_cap:"$381.7 B",
        sales: "$86.8 B",
        profit: "20.6%",
        num_empl: "NA"
    },
    {
        name: "Symetra Financial",
        market_cap:"$2.7 B",
        sales: "$2.2 B",
        profit: "6.8%",
        num_empl: "NA"
    },
    {
        name: "Micron Technology",
        market_cap:"$37.6 B",
        sales: "$16.4 B",
        profit: "20.7%",
        num_empl: "NA"
    },
    {
        name: "F5 Networks",
        market_cap:"$9.5 B",
        sales: "$1.7 B",
        profit: "23.7%",
        num_empl: "NA"
    },
    {
        name: "Expedia",
        market_cap:"$10.8 B",
        sales: "$5.8 B",
        profit: "9.3%",
        num_empl: "NA"
    },
    {
        name: "Nautilus",
        market_cap:"$476 B",
        sales: "$274.4 B",
        profit: "35.9%",
        num_empl: "NA"
    },
    {
        name: "Heritage Financial",
        market_cap:"$531 B",
        sales: "$137.6 B",
        profit: "8.8%",
        num_empl: "NA"
    },
    {
        name: "Cascade Microtech",
        market_cap:"$239 B",
        sales: "$136 B",
        profit: "11.3%",
        num_empl: "NA"
    },
    {
        name: "Nike",
        market_cap:"$83.1 B",
        sales: "$27.8 B",
        profit: "19%",
        num_empl: "NA"
    },
    {
        name: "Alaska Air Group",
        market_cap:"$7.9 B",
        sales: "$5.4 B",
        profit: "14.1%",
        num_empl: "NA"
    },
];
var k = '<tbody>'
for(i = 0;i < stock.length; i++){
    k+= '<tr>';
    k+= '<td>' + stock[i].name + '</td>';
    k+= '<td>' + stock[i].market_cap + '</td>';
    k+= '<td>' + stock[i].sales + '</td>';
    k+= '<td>' + stock[i].profit + '</td>';
    k+= '<td>' + stock[i].num_empl + '</td>';
    k+= '</tr>';
}
k+='</tbody>';
document.getElementById('tableData').innerHTML = k;